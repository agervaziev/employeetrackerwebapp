﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using EmployeeTrackerWebApp.Data.Models;
using EmployeeTrackerWebApp.Mappers;
using EmployeeTrackerWebApp.Models;
using EmployeeTrackerWebApp.Services;
using EmployeeTrackerWebApp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EmployeeTrackerWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEmployeeService employeeService;
        private readonly IModelMapper<ICollection<Employee>, ICollection<EmployeeModel>> employeeCollectionMapper;


        public HomeController(ILogger<HomeController> logger, IEmployeeService employeeService, IModelMapper<ICollection<Employee>, ICollection<EmployeeModel>> employeeCollectionMapper)
        {
            _logger = logger;
            this.employeeService = employeeService ?? throw new ArgumentNullException(nameof(employeeService));
            this.employeeCollectionMapper = employeeCollectionMapper ?? throw new ArgumentNullException(nameof(employeeCollectionMapper));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetDate(DateTime? date)
        {
            if (date == null)
            {
                TempData["Message"] = "Please select date";
                return View("Index");
            }

            string url = "http://localhost:51396/api/clients/subscribe?date=" + date.Value.ToString("yyyy-MM-dd") + "&callback=http://localhost:32508/home/Receiver";
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Headers.Add("Accept-Client", "Fourth-Monitor");

            var response = new HttpClient().Send(request);
            if (response.StatusCode.Equals(StatusCode(200)))
            {
                TempData["Message"] = "Operation successful!";
            }
            else
            {
                TempData["Message"] = "Operation likely to have been unsuccessful!";
            }

            return View("Index");
        }

        [HttpPost]
        public IActionResult Receiver([FromBody] IList<EmployeeModel> data)
        {
            if (!Request.Headers.TryGetValue("X-Fourth-Token", out var token))
                return Unauthorized();

            var employees = employeeCollectionMapper.MapFrom(data);
            employeeService.InsertEmployeesInDatabase(employees);

            return Ok();
        }

        public IActionResult DisplayEmployeeTable()
        {
            var employeeEntities = employeeService.GetAllEmployessFromDatabase();
            var model = new EmployeeTableViewModel();
            var employeeList = new List<EmployeeModel>();
            employeeEntities.ToList().ForEach(x => employeeList.Add(new EmployeeModel { EmployeeId = x.EmployeeId, When = x.When }));
            model.Employees = employeeList;
            return View("EmployeeTable", model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

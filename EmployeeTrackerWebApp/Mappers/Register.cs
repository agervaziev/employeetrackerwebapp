﻿using System.Collections.Generic;
using EmployeeTrackerWebApp.Data.Models;
using EmployeeTrackerWebApp.Models;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeTrackerWebApp.Mappers
{
    public static class MapperRegistrationEmployee
    {
        public static IServiceCollection AddCustomMappersEmployee(this IServiceCollection services)
        {
            services.AddSingleton<IModelMapper<Employee, EmployeeModel>, EmployeeModelMapper>();
            services.AddSingleton<IModelMapper<ICollection<Employee>, ICollection<EmployeeModel>>, EmployeeCollectionModelMapper>();
            return services;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using EmployeeTrackerWebApp.Data.Models;
using EmployeeTrackerWebApp.Models;

namespace EmployeeTrackerWebApp.Mappers
{
    public class EmployeeCollectionModelMapper : IModelMapper<ICollection<Employee>, ICollection<EmployeeModel>>
    {
        private readonly IModelMapper<Employee, EmployeeModel> employeeMapper;

        public EmployeeCollectionModelMapper(IModelMapper<Employee, EmployeeModel> employeeMapper)
        {
            this.employeeMapper = employeeMapper ?? throw new ArgumentNullException(nameof(employeeMapper));
        }

        public ICollection<Employee> MapFrom(ICollection<EmployeeModel> entityList)
        {
            var result = new List<Employee>();
            entityList.ToList().ForEach(x => result.Add(employeeMapper.MapFrom(x)));
            return result;
        }
    }
}

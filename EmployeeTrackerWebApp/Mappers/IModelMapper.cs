﻿namespace EmployeeTrackerWebApp.Mappers
{
    public interface IModelMapper<TModel, TEntity>
    {
        TModel MapFrom(TEntity entity);
    }
}

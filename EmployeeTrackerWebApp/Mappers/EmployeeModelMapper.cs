﻿using EmployeeTrackerWebApp.Data.Models;
using EmployeeTrackerWebApp.Models;

namespace EmployeeTrackerWebApp.Mappers
{
    public class EmployeeModelMapper : IModelMapper<Employee, EmployeeModel>
    {
        public Employee MapFrom(EmployeeModel model)
            => new Employee
            {
                EmployeeId = model.EmployeeId,
                When = model.When,
            };
    }
}

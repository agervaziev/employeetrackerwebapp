﻿using System.Collections.Generic;
using EmployeeTrackerWebApp.Models;

namespace EmployeeTrackerWebApp.ViewModels
{
    public class EmployeeTableViewModel
    {
        public IEnumerable<EmployeeModel> Employees { get; set; }
    }
}

﻿using System.Text.Json.Serialization;

namespace EmployeeTrackerWebApp.Models
{
    public class EmployeeModel
    {
        [JsonPropertyName("EmployeeId")]
        public int EmployeeId { get; set; }
        [JsonPropertyName("When")]
        public string When { get; set; }
    }
}

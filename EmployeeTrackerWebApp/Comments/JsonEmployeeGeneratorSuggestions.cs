﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JsonEmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] roles = new string[] { "Junior Developer", "Semi Senior Developer", "Senior Developer", "Principal", "Team Leader" };
            string[] teams = new string[] { "Platform", "Sales", "Billing", "Mirage" };


            var generator = new Random();
            var all_lines_in_file = File.ReadAllLines("employees.txt").ToArray();
            var employees = new List<JsonEmployee>();
            //var staffMembers = new List<StaffMember>();
            for (int i = 0; i < all_lines_in_file.Length; i++)
            {
                //string name = all_lines_in_file[i].Split('\t')[0];
                //string surname = all_lines_in_file[i].Split('\t')[1];
                //string email = all_lines_in_file[i].Split('\t')[2];

                //if (i < 11)
                //{
                //    var manager = new Manager(i, name, surname, email, generator.Next(11));
                //    staffMembers.Add(manager);
                //    continue;
                //}

                //var employee = new Employee(i, name, surname, email);
                //employee.AssignRole(roles);
                //employee.AssignTeam(teams);
                //staffMembers.Add(employee);

                JsonEmployee e = new JsonEmployee();
                e.Id = i;
                e.Name = all_lines_in_file[i].Split('\t')[0];
                e.SurName = all_lines_in_file[i].Split('\t')[1];
                e.Email = all_lines_in_file[i].Split('\t')[2];
                e.Age = generator.Next(18, 66);
                if (i < 11)
                {
                    e.Role = "Manager";
                    e.Teams = new List<string>();
                }
                else
                {
                    e.ManagerId = generator.Next(11);
                    e.Role = roles[generator.Next(4)];
                    int count = generator.Next(1, 4);
                    var employeeTeams = new List<string>();
                    for (int j = 0; j < count; ++j)
                    {
                        employeeTeams.Add(teams[generator.Next(4)]);
                    }
                    e.Teams = employeeTeams;
                }

                employees.Add(e);
            }
            var jsonFile = File.CreateText("employees.json");
            jsonFile.WriteLine("[");

            //foreach (var staffMember in staffMembers)
            //{
            //    jsonFile.WriteLine(staffMember.DisplayString());
            //}

            for (int i = 0; i < employees.Count; ++i)
            {
                var jsonEmployee = employees[i];
                string str =
                    "{{\"Id\":{7},\"ManagerId\":{0},\"Age\":{1},\"Teams\":[{2}],\"Role\":\"{3}\",\"Email\":\"{4}\",\"SurName\":\"{5}\",\"Name\":\"{6}\"}}";
                if (i != employees.Count - 1)
                    str += ",";
                var formattedEmployeed = string.Format(str,
                    jsonEmployee.ManagerId.HasValue ? jsonEmployee.ManagerId.ToString() : "null",
                    jsonEmployee.Age,
                    string.Join(",", jsonEmployee.Teams.Select(x => "\"" + x + "\"")),
                    jsonEmployee.Role,
                    jsonEmployee.Email,
                    jsonEmployee.SurName,
                    jsonEmployee.Name,
                    jsonEmployee.Id);
                jsonFile.WriteLine(formattedEmployeed);
            }
            jsonFile.WriteLine("]");
            jsonFile.Flush();
        }

    }

    internal class JsonEmployee
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Role { get; set; }
        public int? ManagerId { get; set; }
        public List<string> Teams { get; set; }
        public int Id { get; set; }
    }
}

//public abstract class StaffMember
//{
//    public StaffMember(int id, string name, string surname, string email)
//    {
//        Id= id;
//        Name = name;
//        SurName = surname;
//        Email = email;
//        Age = new Random().Next(18, 66);
//    }

//    public string Name { get; set; }
//    public string SurName { get; set; }
//    public string Email { get; set; }
//    public int Age { get; set; }
//    public string Role { get; set; }
//    public int Id { get; set; }

//    public abstract string DisplayString();
//}

//public class Manager : StaffMember
//{
//    public Manager(int id, string name, string surname, string email, int managerId) : base(id, name, surname, email)
//    {
//        ManagerId = managerId;
//        Role = "Manager";
//    }

//    public int ManagerId { get; set; }

//    public override string DisplayString()
//    {
//        return $"{{\"Id\":{Id},\"ManagerId\":{ManagerId},\"Age\":{Age},\"Teams\":[0],\"Role\":\"{Role}\",\"Email\":\"{Email}\",\"SurName\":\"{SurName}\",\"Name\":\"{Name}\"}}";
//    }
//}

//public class Employee : StaffMember
//{
//    public Employee(int id, string name, string surname, string email) : base(id, name, surname, email)
//    {
//    }

//    public List<string> Teams { get; set; }

//    public override string DisplayString()
//    {
//        return $"{{\"Id\":{Id},\"ManagerId\":0,\"Age\":{Age},\"Teams\":[{string.Join(", ", Teams)}],\"Role\":\"{Role}\",\"Email\":\"{Email}\",\"SurName\":\"{SurName}\",\"Name\":\"{Name}\"}}";
//    }

//    public void AssignRole(string[] roles)
//    {
//        Role = roles[new Random().Next(4)];
//    }

//    public void AssignTeam(string[] teams)
//    {
//        var generator = new Random();
//        int count = generator.Next(1, 4);
//        var employeeTeams = new List<string>();
//        for (int j = 0; j < count; ++j)
//        {
//            employeeTeams.Add(teams[generator.Next(4)]);
//        }

//        Teams = employeeTeams;
//    }
//}


﻿using EmployeeTrackerWebApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeTrackerWebApp.Data.DAL
{
    public class EmployeeRepository : IEmployeeRepository, IDisposable
    {
        private EmployeeContext context;
        private bool disposed = false;

        public EmployeeRepository(EmployeeContext context)
        {
            this.context = context;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return context.Employees.ToList();
        }

        public void InsertEmployees(IEnumerable<Employee> employees)
        {
            using (context)
            {
                context.Database.OpenConnection();
                try
                {
                    context.AddRange(employees);
                    context.SaveChanges();
                }
                finally
                {
                    context.Database.CloseConnection();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}

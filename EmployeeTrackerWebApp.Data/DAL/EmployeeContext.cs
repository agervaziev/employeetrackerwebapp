﻿using EmployeeTrackerWebApp.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeTrackerWebApp.Data.DAL
{
    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public EmployeeContext(DbContextOptions<EmployeeContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=EmployeeTrackerWebApp.Data;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
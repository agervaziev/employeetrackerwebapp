﻿using EmployeeTrackerWebApp.Data.Models;

namespace EmployeeTrackerWebApp.Data.DAL
{
    public interface IEmployeeRepository : IDisposable
    {
        IEnumerable<Employee> GetEmployees();
        void InsertEmployees(IEnumerable<Employee> employees);
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeTrackerWebApp.Data.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string When { get; set; }
    }
}

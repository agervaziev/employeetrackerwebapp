﻿using EmployeeTrackerWebApp.Data.DAL;
using EmployeeTrackerWebApp.Data.Models;

namespace EmployeeTrackerWebApp.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository repository;

        public EmployeeService(IEmployeeRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public ICollection<Employee> GetAllEmployessFromDatabase()
        {
            var employeeEntities = repository.GetEmployees().ToList();
            return employeeEntities.ToList();
        }

        public void InsertEmployeesInDatabase(ICollection<Employee> employees)
        {
            repository.InsertEmployees(employees);
        }
    }
}

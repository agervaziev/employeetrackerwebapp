﻿using EmployeeTrackerWebApp.Data.Models;

namespace EmployeeTrackerWebApp.Services
{
    public interface IEmployeeService
    {
        ICollection<Employee> GetAllEmployessFromDatabase();

        void InsertEmployeesInDatabase(ICollection<Employee> employees);
    }
}
